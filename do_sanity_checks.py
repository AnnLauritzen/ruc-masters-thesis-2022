#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Step 3: check your sanity
"""
import numpy as np
import numpy.linalg as LA
import numpy.polynomial.polynomial as poly
from scipy.integrate import solve_ivp
from timeit import default_timer as timer
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import MultipleLocator

from modules import hpa  # home made module
from modules import hack  # home made module
from modules import check  # home made module
from modules import plot  # home made module


# %% initialise
trap_reg = hpa.trapping_region()
Gamma = np.load('data/Gamma.npy')
X0 = np.load('data/X0.npy')
X = np.load('data/X.npy')
x = X[1:]
m = hpa.parameters()['m']
T = hpa.parameters()['T']
t0 = int(X0[0])
tr = len(X[0])
JPhi = np.load('data/JPhi.npy')
Jphi = JPhi[1:, 1:]
F = np.load('data/F.npy')
JF = np.load('data/JF.npy')
Jf = JF[1:, 1:]


# %% get F(X0)
FX0 = hpa.vector_field(t0, X0)
if np.all(FX0 == F[:, t0]) is False:
    print('We got the wrong F(X0)!')


# %% sanity check on JPhi (should be close to 0)
JPhi_check = check.sanity_check_JPhi(JPhi, FX0, X)
plot.jphi_check(X[0], JPhi_check.T, 'Sanity check on JPhi')


# %% check that F'=JF*F
xz0 = np.array([X0, FX0]).reshape(8)
print("\nGet solution X and Z simultaniously")
ping = timer()
# d(x,z)/dt where dx/dt = f(x) and dz/dt = JF*z
sol = hack.solve(check.dxzdt, X[0], xz0)
print(f"{timer() - ping} seconds needed for that")
what = np.allclose(sol.y[4:], hpa.vector_field(sol.y[:4][0], sol.y[:4]),
                   rtol=1e-05, atol=1e-06)
if what is False:
    print("F does not solve the Z'=JF*Z differential equation!")


# %%% sanity check on JF (should be close to 0)
JF_check = check.sanity_check_JF(F, JF)
plot.jf_check(X[0], JF_check, 'Sanity check on JF')
