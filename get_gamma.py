#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Step 1: get the periodic orbit Gamma
"""
import numpy as np
import numpy.linalg as lin
import numpy.polynomial.polynomial as poly
from scipy.integrate import solve_ivp
from timeit import default_timer as timer

from modules import hpa  # home made module
from modules import hack  # home made module
from modules import plot  # home made module


# time is in minutes
m = hpa.parameters()['m']
# period of gamma in minutes
T = hpa.parameters()['T']
# initial time in minutes
t0 = 3*m


# %% get solution X and JPhi simultaniously, with random X0
X0_r = np.array([t0, 0, 10, 20])
print("\nget solution X and JPhi simultaniously, with random X0")
ping = timer()
days = 10
X_r, JPhi_r, norm_r = hack.flow(days, X0_r)
print(f"{timer() - ping} seconds needed for that")
x_r = X_r[1:]
Jphi_r = JPhi_r[1:, 1:]
plot.phi(X_r, 'Solution with random initial value')
plot.dist(norm_r, 'Solution with random initial value')


# %% get Gamma
Gamma = np.copy(X_r[:, -T-t0:-t0])
Gamma[0] = Gamma[0]-T*(days-1)
plot.phi(Gamma, 'Periodic solution Gamma')

np.save('data/Gamma', Gamma)
