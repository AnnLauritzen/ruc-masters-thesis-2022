#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Step 5: investigate JF
"""
from timeit import default_timer as timer
import numpy as np
import numpy.linalg as LA
import numpy.polynomial.polynomial as poly
import matplotlib.pyplot as plt

from modules import hpa  # home made module
from modules import hack  # home made module
from modules import plot  # home made module


# %% initialise
trap_reg = hpa.trapping_region()
Gamma = np.load('data/Gamma.npy')
X0 = np.load('data/X0.npy')
X = np.load('data/X.npy')
x = X[1:]
T = hpa.parameters()['T']
m = int(T/24)
t0 = int(X0[0])
tr = len(X[0])
JPhi = np.load('data/JPhi.npy')
Jphi = JPhi[1:, 1:]
F = np.load('data/F.npy')
JF = np.load('data/JF.npy')
Jf = JF[1:, 1:]


# %% check basic properties
hack.investigate_matrix(X[0], JF, r"J\vec{F}(\vec{X})", "JF")
hack.investigate_matrix(X[0], Jf, r"J\vec{f}(\vec{X})", "Jf")


# %% get eigenvalues (n) and eigenvectors (N) of JF
[JF_l, JF_v] = LA.eig(np.transpose(JF, axes=(2, 0, 1)))

JF_v, inner_product = hack.make_eigvec_cont(JF_v)
JF_v = hack.make_eigvec_orthogonal(JF_v)

n1 = JF_l[:, 0]
N1 = JF_v[:, :, 0]
n2 = JF_l[:, 1]
N2 = JF_v[:, :, 1]
n3 = np.real(JF_l[:, 2])
N3 = np.real(JF_v[:, :, 2])
n4 = np.real(JF_l[:, 3])
N4 = np.real(JF_v[:, :, 3])

no_of_rotations_for_N1 = np.trapz(np.imag(n1)/(2*np.pi))
print("number of rotations of N1: ", no_of_rotations_for_N1)


# %% plot things
plot.plot_2d("inner product between vectors")
plt.plot(X[0]/m, np.real(inner_product[:, 0]),
          '-', label=r"$\Re(<\vec{N}_1(t), \vec{N}_1(t-1)>)$")
plt.plot(X[0]/m, np.imag(inner_product[:, 0]),
          '--', label=r"$\Im(<\vec{N}_1(t), \vec{N}_1(t-1)>)$")
plt.legend()
plt.savefig("images/JF/inner product between vectors N1(t) and N1(t-1)")

plot.matrix_eigenvalues_re(X[0], JF_l, r"\nu", r"J\vec{F}", "JF")
plt.savefig("images/JF/eigenvalues of JF real.png")
plot.matrix_eigenvalues_im(X[0], JF_l, r"\nu", r"J\vec{F}", "JF")
plt.savefig("images/JF/eigenvalues of JF imag.png")

plot.matrix_eigenvectors_re(X[0], JF_v, r"\vec{N}", r"J\vec{F}", "JF")
plt.savefig("images/JF/eigenvectors of JF real.png")
plot.matrix_eigenvectors_im(X[0], JF_v, r"\vec{N}", r"J\vec{F}", "JF")
plt.savefig("images/JF/eigenvectors of JF imag.png")


# %% get angle between Re(N1) and Im(N1)
RN1_on_IN1 = np.einsum('ji,ji->j', np.real(N1), np.imag(N1))  # real inner product

plot.plot_2d("cosine of angle between vectors")
plt.plot(X[0]/m, RN1_on_IN1/LA.norm(np.real(N1))/LA.norm(np.imag(N1)),
          '-', label=r"$\dfrac{<\Re(\vec{N}_1(t)), \Im(\vec{N}_1(t))>}"\
              r"{||\Re(\vec{N}_1(t))|| \cdot ||\Im(\vec{N}_1(t))||}$")
plt.legend()
plt.savefig("images/JF/angle between vectors Re(N1)-Im(N1).png")


# %% get angle between N1 and N2
N1_on_N2 = np.einsum('ji,ji->j', N1, np.conj(N2))  # complex inner product

plot.plot_2d("cosine of angle between vectors")
plt.plot(X[0]/m, np.real(N1_on_N2),
         '-', label=r"$\Re <\vec{N}_1(t), \vec{N}_2(t)>$")
plt.plot(X[0]/m, np.imag(N1_on_N2),
         '-', label=r"$\Im <\vec{N}_1(t), \vec{N}_2(t)>$")
plt.legend()
plt.savefig("images/JF/angle between vectors N1-N2.png")


# %% plot norms of eigenvectors
plot.plot_2d(r"length eigenvectors of $J\vec{F}$")
plt.plot(X[0]/m, LA.norm(N1, axis=1),
         '-', label=r"$||\vec{N}_1(t)||$")

plt.plot(X[0]/m, LA.norm(np.real(N1), axis=1),
         '--', label=r"$||\Re \vec{N}_1(t)||$")
plt.plot(X[0]/m, LA.norm(np.imag(N1), axis=1),
         ':', label=r"$||\Im \vec{N}_1(t)||$")

plt.plot(X[0]/m, LA.norm(N2, axis=1),
         '-', label=r"$||\vec{N}_2(t)||$")
plt.plot(X[0]/m, LA.norm(N3, axis=1),
         '-', label=r"$||\vec{N}_3(t)||$")
plt.plot(X[0]/m, LA.norm(N4, axis=1),
         '-', label=r"$||\vec{N}_4(t)||$")
plt.legend()
plt.savefig("images/JF/length eigenvectors of JF.png")


# %% plot Re(N1)-Im(N1)-plane
fig = plt.figure(figsize=(7, 5), dpi=(300))
plt.title(r"$\left( \Re(\vec{N_1}(t)), \Im(\vec{N_1}(t)) \right)$-plane")
plt.plot(np.real(N1), np.imag(N1), '-')
plt.plot(np.real(N1)[0], np.imag(N1)[0], '*', label="start")
plt.plot(np.real(N1)[-1], np.imag(N1)[-1], 'x', label="stop")
plt.grid()
plt.legend()
plt.xlabel(r"$\Re(\vec{N_1})_t$")
plt.ylabel(r"$\Im(\vec{N_1})_t$")
plt.savefig("images/JF/Re(N1)-Im(N1)-plane.png")

fig = plt.figure(figsize=(7, 5), dpi=(300))
plt.title(r"$\left( \Re(\vec{N_2}(t)), \Im(\vec{N_2}(t)) \right)$-plane")
plt.plot(np.real(N2), np.imag(N2), '-')
plt.plot(np.real(N2)[0], np.imag(N2)[0], '*', label="start")
plt.plot(np.real(N2)[-1], np.imag(N2)[-1], 'x', label="stop")
plt.grid()
plt.legend()
plt.xlabel(r"$\Re(\vec{N_2})_t$")
plt.ylabel(r"$\Im(\vec{N_2})_t$")
plt.savefig("images/JF/Re(N2)-Im(N2)-plane.png")


# %% investigate projections onto N
Ft, P2_t, rP2_t, iP2_t, P3_t, P4_t = np.load('data/Pt.npy')

F_on_N = hack.v_projections_on_V_basis(Ft, JF_v)
P2_on_N = hack.v_projections_on_V_basis(P2_t, JF_v)
P3_on_N = hack.v_projections_on_V_basis(P3_t, JF_v)
P4_on_N = hack.v_projections_on_V_basis(P4_t, JF_v)

# %%% plot
plot.V_plane_projections_grid(X[0], F_on_N, r"\vec{P}_1", r"\vec{N}")
plt.savefig("images/JF/P1 curve in N plane")
plot.V_plane_projections_grid(X[0], P2_on_N, r"\vec{P}_2", r"\vec{N}")
plt.savefig("images/JF/P2 curve in N plane")
plot.V_plane_projections_grid(X[0], P3_on_N, r"\vec{P}_3", r"\vec{N}")
plt.savefig("images/JF/P3 curve in N plane")
plot.V_plane_projections_grid(X[0], P4_on_N, r"\vec{P}_4", r"\vec{N}")
plt.savefig("images/JF/P4 curve in N plane")
# %%%
plot.V_plane_projections_grid2(F_on_N/LA.norm(Ft, axis=1),
                               r"\vec{P}_1", r"\vec{N}")
plt.savefig("images/JF/normalised P1 curve in N plane")
plot.V_plane_projections_grid2(P2_on_N/LA.norm(P2_t, axis=1),
                               r"\vec{P}_2", r"\vec{N}")
plt.savefig("images/JF/normalised P2 curve in N plane")
plot.V_plane_projections_grid2(P3_on_N/LA.norm(P3_t, axis=1),
                               r"\vec{P}_3", r"\vec{N}")
plt.savefig("images/JF/normalised P3 curve in N plane")
plot.V_plane_projections_grid2(P4_on_N/LA.norm(P4_t, axis=1),
                               r"\vec{P}_4", r"\vec{N}")
plt.savefig("images/JF/normalised P4 curve in N plane")


# %% get modulus and argument
plot.matrix_elements3(X[0], F_on_N, r"\vec{P}_1")
plt.savefig("images/JF/P1 curve in N plane modulus and argument")
plot.matrix_elements3(X[0], P2_on_N, r"\vec{P}_2")
plt.savefig("images/JF/P2 curve in N plane modulus and argument")
plot.matrix_elements3(X[0], P3_on_N, r"\vec{P}_3")
plt.savefig("images/JF/P3 curve in N plane modulus and argument")
plot.matrix_elements3(X[0], P4_on_N, r"\vec{P}_4")
plt.savefig("images/JF/P4 curve in N plane modulus and argument")


# %% make matrix of N inner products
N = np.array([N1, N2, N3, N4])
C = np.empty(shape=JF.shape, dtype=np.complex128)
for r in range(4):
    for c in range(4):
        C[r, c] = np.einsum('ji,ji->j', N[r], np.conj(N[c]))

plot.matrix_elements2(X[0], np.real(C), r"N")
plt.savefig("images/JF/columns of N real.png")
plot.matrix_elements2(X[0], np.imag(C), r"N")
plt.savefig("images/JF/columns of N imag.png")
