#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Investigate JF in N basis
"""
from timeit import default_timer as timer
import numpy as np
import numpy.linalg as LA
import numpy.polynomial.polynomial as poly
import matplotlib.pyplot as plt

from modules import hpa  # home made module
from modules import hack  # home made module
from modules import plot  # home made module


# %% initialise
X0 = np.load('data/X0.npy')
X = np.load('data/X.npy')
T = hpa.parameters()['T']
m = int(T/24)
t0 = int(X0[0])
tr = len(X[0])
F = np.load('data/F.npy')
JF = np.load('data/JF.npy')


# %% get eigenvalues (n) and eigenvectors (N) of JF
[n, N] = LA.eig(np.transpose(JF, axes=(2, 0, 1)))

N, inner_product = hack.make_eigvec_cont(N)
N = hack.make_eigvec_orthogonal(N)

P1, P2, rP2, iP2, P3, P4 = np.load('data/Pt.npy')


# %% make matrix of N basis
P1_in_N = np.einsum('ijk,ik->ij' , LA.inv(N), P1)/LA.norm(P1, axis=1)[:, np.newaxis]
P2_in_N = np.einsum('ijk,ik->ij' , LA.inv(N), P2)/LA.norm(P2, axis=1)[:, np.newaxis]
P3_in_N = np.einsum('ijk,ik->ij' , LA.inv(N), P3)/LA.norm(P3, axis=1)[:, np.newaxis]
P4_in_N = np.einsum('ijk,ik->ij' , LA.inv(N), P4)/LA.norm(P4, axis=1)[:, np.newaxis]

plot.V_plane_projections_grid3(P1_in_N, r"\vec{F}", r"\vec{N}")
plt.savefig("images/JF/P1 curve in N basis")
plot.V_plane_projections_grid3(P2_in_N, r"\vec{P}_2", r"\vec{N}")
plt.savefig("images/JF/P2 curve in N basis")
plot.V_plane_projections_grid3(P3_in_N, r"\vec{P}_3", r"\vec{N}")
plt.savefig("images/JF/P3 curve in N basis")
plot.V_plane_projections_grid3(P4_in_N, r"\vec{P}_4", r"\vec{N}")
plt.savefig("images/JF/P4 curve in N basis")


# %% scaled

n_t = np.cumsum(n, axis=0)

a = np.einsum('ij,i->ij', P1_in_N, np.exp(-n_t[:, 0]))
b = np.einsum('ij,i->ij', P2_in_N, np.exp(-n_t[:, 1]))
c = np.einsum('ij,i->ij', P3_in_N, np.exp(-n_t[:, 2]))
d = np.einsum('ij,i->ij', P4_in_N, np.exp(-n_t[:, 3]))

plot.V_plane_projections_grid3(a, r"\vec{F}", r"\vec{N}")
plt.savefig("images/JF/P1 curve in N basis")
plot.V_plane_projections_grid3(b, r"\vec{P}_2", r"\vec{N}")
plt.savefig("images/JF/P2 curve in N basis")
plot.V_plane_projections_grid3(c, r"\vec{P}_3", r"\vec{N}")
plt.savefig("images/JF/P3 curve in N basis")
plot.V_plane_projections_grid3(d, r"\vec{P}_4", r"\vec{N}")
plt.savefig("images/JF/P4 curve in N basis")
