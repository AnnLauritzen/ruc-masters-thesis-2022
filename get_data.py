#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Step 2: get lots of data
"""
import numpy as np
import numpy.linalg as lin
import numpy.polynomial.polynomial as poly
from scipy.integrate import solve_ivp
from timeit import default_timer as timer

from modules import hpa  # home made module
from modules import hack  # home made module
from modules import plot  # home made module


# time is in minutes
m = 60
# period of gamma in minutes
T = hpa.parameters()['T']
# initial time in minutes
t0 = 0*m


# %% get gamma
gamma = np.load('data/Gamma.npy')


# %% get solution X and JPhi simultaniously, with X0 in gamma
X0 = gamma[:, t0]
if X0[0] == t0 is False:
    print('We got the wrong X0!')
print("\nget solution X and JPhi simultaniously, with X0 in gamma")
ping = timer()
days = 1
X, JPhi, norm = hack.flow(days, X0)
print(f"{timer() - ping} seconds needed for that")
plot.phi(X, 'Solution with initial value in gamma')
if days > 1:
    plot.dist(norm, 'Solution with initial value in gamma')

np.save('data/X0', X0)
np.save('data/X', X)
np.save('data/JPhi', JPhi)


# %% get F and JF
F = hpa.vector_field(X[0], X)
JF = hpa.derivative_of_vector_field(X[0], X)
np.save('data/F', F)
np.save('data/JF', JF)
