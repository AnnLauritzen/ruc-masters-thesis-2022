#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Optional: investigate the symmetric matrix, even though it's unhelpful
"""
from timeit import default_timer as timer
import numpy as np
import numpy.linalg as LA
import numpy.polynomial.polynomial as poly
import matplotlib.pyplot as plt

from modules import hpa  # home made module
from modules import hack  # home made module
from modules import plot  # home made module


# %% initialise
trap_reg = hpa.trapping_region()
Gamma = np.load('data/Gamma.npy')
X0 = np.load('data/X0.npy')
X = np.load('data/X.npy')
x = X[1:]
T = hpa.parameters()['T']
m = int(T/24)
t0 = int(X0[0])
tr = len(X[0])
JPhi = np.load('data/JPhi.npy')
Jphi = JPhi[1:, 1:]
F = np.load('data/F.npy')
JF = np.load('data/JF.npy')
Jf = JF[1:, 1:]


# %% investigate A=JF+JF.T matrix
A = JF + np.transpose(JF, axes=(1, 0, 2))
hack.investigate_matrix(X[0], A, r"(J\vec{F}+J\vec{F}^T)(\vec{X}(t))", "JF+JF.T")


# %% get eigenvals (l) and eigenvecs (v) of A
[A_l, A_v] = hack.get_sorted_eigen(A)
A_l = np.transpose(A_l)
A_l = np.array(A_l)
A_v = np.array(A_v)
A_v = np.transpose(A_v, axes=(1, 2, 0))
# TODO: "sorted" is not well defined for time series

for i in range(1, tr):
    for j in range(4):
        if LA.norm(A_v[i, :, j] - A_v[i-1, :, j]) > 0.5:
            A_v[i, :, j] = -1*A_v[i, :, j]

plot.matrix_eigenvalues_re(X[0], A_l, r"\lambda",
                           r"J\vec{F}+J\vec{F}^T", "JF+JF.T")
plt.plot(X[0]/m, np.real(A_l[:, 0]+A_l[:, 3]),
         '.', label=r'Re($\lambda_1+\lambda_4$)')
plt.legend()
plt.savefig("images/JF+JF.T/eigenvalues of JF+JF.T real.png")

plot.matrix_eigenvalues_im(X[0], A_l, r"\lambda",
                           r"J\vec{F}+J\vec{F}^T", "JF+JF.T")
plt.savefig("images/JF+JF.T/eigenvalues of JF+JF.T imag.png")

plot.matrix_eigenvectors_re(X[0], A_v, r"\vec{\Lambda}",
                            r"J\vec{F}+J\vec{F}^T", "JF+JF.T")
plt.savefig("images/JF+JF.T/eigenvectors of JF+JF.T real.png")

plot.matrix_eigenvectors_im(X[0], A_v, r"\vec{\Lambda}",
                            r"J\vec{F}+J\vec{F}^T", "JF+JF.T")
plt.savefig("images/JF+JF.T/eigenvectors of JF+JF.T imag.png")


# %% get projections of A_v[0], A_v[3], ut and vt

Ft, P2_t, rP2_t, iP2_t, P3_t, P4_t = np.load('data/Pt.npy')

u_on_L1, u_on_L2, u_on_L3, u_on_L4 = hack.v_projections_on_V_basis(rP2_t, A_v)
v_on_L1, v_on_L2, v_on_L3, v_on_L4 = hack.v_projections_on_V_basis(iP2_t, A_v)
F_on_L1, F_on_L2, F_on_L3, F_on_L4 = hack.v_projections_on_V_basis(Ft, A_v)
o_on_L1, o_on_L2, o_on_L3, o_on_L4 = hack.v_projections_on_V_basis(P4_t, A_v)

plot.v_projections(X[0], rP2_t, u_on_L1, u_on_L2, u_on_L3, u_on_L4, r"\vec{u}")
plot.v_projections(X[0], iP2_t, v_on_L1, v_on_L2, v_on_L3, v_on_L4, r"\vec{v}")
plot.v_projections(X[0], Ft, F_on_L1, F_on_L2, F_on_L3, F_on_L4, r"\vec{F}")
plot.v_projections(X[0], P4_t, o_on_L1, o_on_L2, o_on_L3, o_on_L4, r"\vec{\omega}")

# %% plot projection on L1-L4 plane

plot.L1L4_plane(u_on_L1, u_on_L4, r"\vec{u}", r"\vec{\Lambda}")
plot.L1L4_plane(v_on_L1, v_on_L4, r"\vec{v}", r"\vec{\Lambda}")
plot.L1L4_plane(F_on_L1, F_on_L4, r"\vec{F}", r"\vec{\Lambda}")
plot.L1L4_plane(o_on_L1, o_on_L4, r"\vec{\omega}", r"\vec{\Lambda}")













# [l, v] = LA.eig(np.transpose(A, axes=(2, 0, 1)))
# diff = l - np.roll(l, 1, axis=0)
# diff_norm = LA.norm(diff, axis=1)

l1, l2, l3, l4, v1, v2, v3, v4 = hack.get_sorted_eigen(A)

# %%% get JPhi(v1(0))
JPhi_v10 = np.matmul(np.transpose(JPhi, axes=(2, 0, 1)), v1[0])
# %%%% get v-projections of JPhi(v1(0))
JPhi_v10_v1_proj = np.empty(tr)
JPhi_v10_v2_proj = np.empty(tr)
JPhi_v10_v3_proj = np.empty(tr)
JPhi_v10_v4_proj = np.empty(tr)
F_v1_proj = np.empty(tr)
F_v2_proj = np.empty(tr)
F_v3_proj = np.empty(tr)
F_v4_proj = np.empty(tr)
for i in range(tr):
    normalize = LA.norm(JPhi_v10[i])
    JPhi_v10_v1_proj[i] = np.inner(JPhi_v10[i], v1[i]) / normalize
    normalize = LA.norm(JPhi_v10[i])
    JPhi_v10_v2_proj[i] = np.inner(JPhi_v10[i], v2[i]) / normalize
    normalize = LA.norm(JPhi_v10[i])
    JPhi_v10_v3_proj[i] = np.inner(JPhi_v10[i], v3[i]) / normalize
    normalize = LA.norm(JPhi_v10[i])
    JPhi_v10_v4_proj[i] = np.inner(JPhi_v10[i], v4[i]) / normalize
    normalize = LA.norm(F[:, i])
    F_v1_proj[i] = np.inner(F[:, i], v1[i]) / normalize
    F_v2_proj[i] = np.inner(F[:, i], v2[i]) / normalize
    F_v3_proj[i] = np.inner(F[:, i], v3[i]) / normalize
    F_v4_proj[i] = np.inner(F[:, i], v4[i]) / normalize


# %% check when solutions start contracting
v = v1[0]
bigger = LA.norm(v) / LA.norm(F[:, t0])
# %%% get JPhi(v) where v is in the tangent space
JPhi_v = np.matmul(np.transpose(JPhi, axes=(2, 0, 1)), v).T
smaller = np.empty(tr)
for i in range(tr):
    smaller[i] = LA.norm(JPhi_v[:, i], axis=0) / LA.norm(F[:, i], axis=0)


# %% plot check thing
hpa.plot('')
plt.plot(X[0]/m, bigger,
         '--', color='k', label=r'$\dfrac{||\vec{v}||}{||\vec{F}(\vec{x}_0)||}$')
plt.plot(X[0]/m, smaller,
         '-', color='k', label=r"$\dfrac{|| J\Phi_{t, \vec{X}_0} \cdot "\
             r"\vec{v} ||}{|| \vec{F}(\vec{\Phi}_{t}(\vec{X}_0)) ||}$")
plt.yscale('log')
plt.legend()
plt.tight_layout()
