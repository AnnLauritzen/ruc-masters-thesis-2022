#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Step 4: investigate JPhi
"""
from timeit import default_timer as timer
import numpy as np
import numpy.linalg as LA
import numpy.polynomial.polynomial as poly
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import MultipleLocator

from modules import hpa  # home made module
from modules import hack  # home made module
from modules import plot  # home made module


# %% initialise
trap_reg = hpa.trapping_region()
Gamma = np.load('data/Gamma.npy')
X0 = np.load('data/X0.npy')
X = np.load('data/X.npy')
x = X[1:]
m = hpa.parameters()['m']
T = hpa.parameters()['T']
t0 = int(X0[0])
tr = len(X[0])
JPhi = np.load('data/JPhi.npy')
Jphi = JPhi[1:, 1:]
F = np.load('data/F.npy')
JF = np.load('data/JF.npy')
Jf = JF[1:, 1:]


# %% get circadian rhythm
C = hpa.circadian(Gamma[0])
dC = hpa.derivative_of_circadian(Gamma[0])
dC2 = C - np.roll(C, 1)
what = np.allclose(dC, dC2, rtol=1e-05, atol=1e-05)
if what is False:
    print('I got the wrong dC/dt!')
plot.circadian(Gamma[0], C, dC, dC2, 'The circadian function')


# %% get F(X0)
FX0 = hpa.vector_field(t0, X0)
if np.all(FX0 == F[:, t0]) is False:
    print('We got the wrong F(X0)!')
plot.vecfield(X, F, 'The vector field along an orbit')


# %% get properties of Jphi and JPhi
hack.investigate_matrix(X[0], Jphi, r"J\vec{\phi}_t(\vec{X}_0)", "Jphi")
hack.investigate_matrix(X[0], JPhi, r"J\vec{\Phi}_t(\vec{X}_0)", "JPhi")
# %%% get some norms of JPhi
I = np.eye(4)
JPhi_norm = np.empty((4, tr))
for i in range(4):
    JPhi_e = np.matmul(np.transpose(JPhi, axes=(2, 0, 1)), I[i])
    JPhi_norm[i] = LA.norm(JPhi_e, axis=(1))
plot.jphi_e_norm(X[0], JPhi_norm, 'Norms of JPhi on basis vectors')
# %%% get slope of 2-norm of Jphi
if tr > T:
    Jphi_norm = LA.norm(Jphi, axis=(0, 1))
    coefs = poly.polyfit(X[0], np.log(Jphi_norm), 1)
    print(f"\nLin reg for log(||Jphi(t)||): {coefs[0]} + {coefs[1]}*t")
    # ffit = poly.polyval(X[0], coefs)


# %% get eigenvalues and eigenvectors of B = JPhi_24
if tr > T:
    B = JPhi[:, :, T]
else:
    B = JPhi[:, :, -1]

[B_l, B_v] = hack.get_sorted_eigen(B)
for i in range(4):  # test if it really is an eigenv
    test1 = np.matmul(B, B_v[i])
    test2 = B_l[i]*B_v[i]
    what = np.allclose(test1, test2, rtol=1e-05, atol=1e-06)
    if what is False:
        print('The eigenvectors are wrong!')

if np.all(np.imag(B_v[0])==0):
    rho1 = np.real(B_l[0])
    P1 = np.real(B_v[0])
    what = np.allclose(P1, FX0, rtol=1e-05, atol=1e-06)
    if what is False:
        print('The eigenvector P1 should have been F(X(0))!')
if np.all(B_v[1] == np.conjugate(B_v[2])):
    rho2 = B_l[1]
    P2 = B_v[1]
    rho3 = B_l[2]
    P3 = B_v[2]
if np.all(np.imag(B_v[3])==0):
    rho4 = np.real(B_l[3])
    P4 = np.real(B_v[3])
# %%% print the eigenv.
np.set_printoptions(precision=4)
print(f"The vector F(X0): {FX0}")
print("\nThe transformation B = JPhi_24 has:")
for i in range(4):
    print(f"eigenvalue rho{i+1}: {np.array(B_l[i])}")
    print(f"with eigenvector P{i+1}: \n{B_v[i]}\n")


# %% transform eigenvectors of B
P2_t = np.matmul(np.transpose(JPhi, axes=(2, 0, 1)), P2)
rP2_t = np.matmul(np.transpose(JPhi, axes=(2, 0, 1)), np.real(P2))
iP2_t = np.matmul(np.transpose(JPhi, axes=(2, 0, 1)), np.imag(P2))
P3_t = np.matmul(np.transpose(JPhi, axes=(2, 0, 1)), P3)
P4_t = np.matmul(np.transpose(JPhi, axes=(2, 0, 1)), P4)

Pt = np.array([F.T, P2_t, rP2_t, iP2_t, P3_t, P4_t])
np.save('data/Pt', Pt)


# %% plot norms of transformed eigenvectors
plot.plot_2d(r"time evolution of eigenvectors of "\
             r"$J\vec{\Phi}_{T, \vec{\Gamma}(0)}$")
plt.plot(X[0]/m, LA.norm(P2_t, axis=1),
         '-', label=r"$||\vec{P}_2(t)||$")
plt.plot(X[0]/m, LA.norm(rP2_t, axis=1),
         '--', label=r"$||\Re\left(\vec{P}_2(t)\right)||$")
plt.plot(X[0]/m, LA.norm(iP2_t, axis=1),
         '--', label=r"$||\Im\left(\vec{P}_2(t)\right)||$")
plt.plot(X[0]/m, LA.norm(F, axis=0),
          '-.', label=r"$||\vec{F}(\vec{X}(t))||$")
plt.plot(X[0]/m, LA.norm(P4_t, axis=1),
           ':', label=r"$||\vec{P}_4(t)||$")
plt.ylim(min(LA.norm(rP2_t, axis=1))*0.6, max(LA.norm(rP2_t, axis=1))*1.4)
plt.yscale('log')
plt.legend()
plt.savefig("images/JPhi/norms of transformed eigenvectors.png")
# %%% plot angle between u and v
rP2_dot_iP2 = np.einsum('ji,ji->j', rP2_t, np.conj(iP2_t))  # complex inner product

plot.plot_2d(r"angle between $\Re\left(\vec{P}_2(t)\right)$ and "\
             r"$\Im\left(\vec{P}_2(t)\right)$")
plt.plot(X[0]/m, rP2_dot_iP2/LA.norm(rP2_t, axis=1)/LA.norm(iP2_t, axis=1),
         label=r"$\frac{<\Re\left(\vec{P}_2(t)\right), \Im\left(\vec{P}_2(t)\right)>}"\
             r"{||\Re\left(\vec{P}_2(t)\right)|| ||\Im\left(\vec{P}_2(t)\right)||}$")
plt.legend()
plt.savefig("images/JPhi/angle between vectors.png")
# %%% plot u-v-plane
fig = plt.figure(figsize=(7, 5), dpi=(300))
plt.title(r"$\left(\Re\left(\vec{P}_2(t)\right),"\
          r"\Im\left(\vec{P}_2(t)\right)\right)$-plane")
plt.plot(rP2_t, iP2_t, '.')
plt.plot(rP2_t[0], iP2_t[0], '*', label="start")
plt.plot(rP2_t[-1], iP2_t[-1], 'x', label="stop")
plt.grid()
plt.legend()
plt.xlabel(r"$\Re\left(\vec{P}_2(t)\right)$")
plt.ylabel(r"$\Im\left(\vec{P}_2(t)\right)$")
plt.savefig("images/JPhi/uv-plane.png")


# %% get alpha functions
alpha_F = hack.alpha(F.T, JF)
alpha_w = hack.alpha(P2_t, JF)
alpha_u = hack.alpha(rP2_t, JF)
alpha_v = hack.alpha(iP2_t, JF)
# %%% plot eigenvector w(t)
plot.plot_2d(r"$\alpha_t(\vec{v}_t) = <\vec{v}_t, J\vec{F}_t\vec{v}_t> / ||\vec{v}_t||^2$")
plt.plot(X[0]/m, alpha_F, label=r"$\alpha_t(\vec{F}(t))$")
plt.plot(X[0]/m, alpha_u,
          '--', label=r"$\alpha_t(\Re\left(\vec{P}_2(t)\right))$")
plt.plot(X[0]/m, alpha_v,
          '-.', label=r"$\alpha_t(\Im\left(\vec{P}_2(t)\right))$")
plt.plot(X[0]/m, alpha_w,
          ':', label=r"$\alpha_t(\vec{P}_4(t))$")
plt.legend()
plt.savefig("images/JPhi/alpha.png")