#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
A model module
"""
import numpy as np
import numpy.linalg as lin
import numpy.polynomial.polynomial as poly
from scipy.integrate import solve_ivp


def parameters():
    params = [4.71e-2,  # a0, basic contribution of CRH
              6.84e12,  # a1, max contribution of CRH
              1.78e9,  # a2, inhibition of CRH
              583,  # mu, half saturation constant of CRH
              2.28e4,  # a3, max contribution of ACTH
              1.77e5,  # a4, inhibition of ACTH
              3.81e-4,  # a5, contribution of CORT
              4.49e-2,  # omega1, elimination of CRH
              2.25e-2,  # omega2, elimination of ACTH
              2.01e-2,  # omega3, elimination of CORT
              0,  # delta, time shift
              300,  # alpha, half saturation point
              5,  # k, steepness of Hill at midpoint
              950,  # beta, half saturation point
              6,  # l, steepness in Hill at midpoint
              0.01,  # epsilon, basic contribution
              0.5217,  # Nc, normalization constant
              24*60,  # T, modulo 24 hours
              60]  # min, unit for time

    params_mik = ['a0', 'a1', 'a2', 'mu', 'a3', 'a4', 'a5',
                  'omega1', 'omega2', 'omega3',
                  'delta', 'alpha', 'k', 'beta', 'l',
                  'epsilon', 'Nc', 'T', 'm']

    parameters = dict(zip(params_mik, params))
    return parameters


def circadian(t, p=parameters()):
    tm = np.mod(t-p['delta'], p['T'])
    akt = (tm**p['k'] + p['alpha']**p['k'])
    blt = ((p['T']-tm)**p['l'] + p['beta']**p['l'])
    C = np.array(
        p['Nc']*(tm**p['k']/akt * (p['T']-tm)**p['l']/blt + p['epsilon'])
        )
    # C = np.ones(np.size(t))*0.1  # use this for a constant C!
    return C


def derivative_of_circadian(t, p=parameters()):
    tm = np.mod(t-p['delta'], p['T'])
    akt = (tm**p['k'] + p['alpha']**p['k'])
    blt = ((p['T']-tm)**p['l'] + p['beta']**p['l'])
    dC = np.array(
        p['Nc']*(
            - p['l']*tm**p['k']*(p['T']-tm)**(p['l']-1) / (akt*blt)
            + p['l']*tm**p['k']*(p['T']-tm)**(2*p['l']-1) / (akt*blt**2)
            + p['k']*tm**(p['k']-1)*(p['T']-tm)**p['l'] / (akt*blt)
            - p['k']*tm**(2*p['k']-1)*(p['T']-tm)**p['l'] / (akt**2*blt)
            )
        )
    # dC = np.ones(np.size(t))*0.0  # use this for a constant C!
    return dC


def vector_field(t, X, p=parameters()):
    X0, X1, X2, X3 = X
    axes = np.size(np.shape(X))
    if axes == 1:
        one = 1
    elif axes == 2:
        one = np.ones(len(X0))
    else:
        print(f'This X has {axes} axes!?')

    C = circadian(X0, p)

    F0 = one
    F1 = p['a0']+C*p['a1']/(1+p['a2']*X3**2)*X1/(p['mu']+X1)-p['omega1']*X1
    F2 = p['a3']*X1/(1+p['a4']*X3)-p['omega2']*X2
    F3 = p['a5']*X2**2 - p['omega3']*X3

    F = np.array([F0, F1, F2, F3])
    return F


def derivative_of_vector_field(t, X, p=parameters()):
    X0, X1, X2, X3 = X
    axes = np.size(np.shape(X))
    if axes == 1:
        one = 1
    elif axes == 2:
        one = np.ones(len(X0))
    else:
        print(f'This X has {axes} axes!?')

    C = circadian(X0, p)
    dC = derivative_of_circadian(X0, p)

    JF0 = np.array([
        one*0., one*0., one*0., one*0.
        ])
    JF1 = np.array([
        dC*p['a1']/(1+p['a2']*X3**2)*X1/(p['mu']+X1),
        (p['a1']*C*p['mu'])
        / ((1+p['a2']*X3**2)*(p['mu'] + X1)**2)-p['omega1'],
        one*0.,
        (-2*X3*p['a2']*p['a1']*X1*C)
        / ((1+p['a2']*X3**2)**2*(p['mu']+X1))
        ])
    JF2 = np.array([
        one*0.,
        p['a3']/(1+p['a4']*X3),
        -one*p['omega2'],
        (-p['a4']*p['a3']*X1)/(1+p['a4']*X3)**2
        ])
    JF3 = np.array([
        one*0.,
        one*0.,
        2*p['a5']*X2,
        -one*p['omega3']
        ])

    JF = np.array([JF0, JF1, JF2, JF3])
    return JF


# TODO: Y u no start on gamma??? 
def derivative_of_JPhi(t, Y, p=parameters()):
    X = Y[:4]
    JPhi = np.reshape(Y[4:], (4, 4))
    F = vector_field(X[0], X)
    JF = derivative_of_vector_field(X[0], X)
    dJPhi = np.matmul(JF, JPhi)
    return np.concatenate((F, dJPhi.reshape(4*4)))


# TODO needs an updated pen-and-paper calculation
def trapping_region(p=parameters()):
    x_min = 0
    y_min = 0
    z_min = 0
    x_max = (p['a0']+p['a1'])/p['omega1']
    y_max = (p['a3']/p['omega2'])*x_max
    z_max = (p['a5']/p['omega3'])*y_max**2
    trapping_region = np.array(
        [[x_min, y_min, z_min],
         [x_max, y_max, z_max]]
        )
    return trapping_region
