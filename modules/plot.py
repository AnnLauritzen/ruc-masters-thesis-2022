#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
A plotting module
"""
import numpy as np
import numpy.linalg as LA
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import MultipleLocator

from modules import hpa  # home made module

# time is in minutes
m = hpa.parameters()['m']
# period of gamma in minutes
T = hpa.parameters()['T']


def plot_2d(title):
    fig = plt.figure(figsize=(7, 5), dpi=(300))
    plt.title(title)
    plt.xlabel(r'$t_0+t$ [hours]')
    plt.gca().xaxis.set_major_locator(MultipleLocator(3))
    plt.grid()
    return fig


def plot_3d(title):
    fig = plt.figure(figsize=(7, 5), dpi=(300))
    fig.suptitle(title)
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.view_init(elev=22, azim=-45)
    ax.set_box_aspect([1, 1, 1])
    return fig, ax


def plot_grid(title):
    fig = plt.figure(figsize=(7, 5), dpi=(300))
    fig.suptitle(title)
    gs = GridSpec(4, 2, figure=fig)
    ax0 = fig.add_subplot(gs[0, 0])
    ax1 = fig.add_subplot(gs[1, 0])
    ax2 = fig.add_subplot(gs[2, 0])
    ax3 = fig.add_subplot(gs[3, 0])
    ax4 = fig.add_subplot(gs[:, 1], projection='3d')

    ax0.grid()
    ax0.xaxis.set_major_locator(MultipleLocator(6))
    ax0.set_xticklabels([])

    ax1.grid()
    ax1.xaxis.set_major_locator(MultipleLocator(6))
    ax1.set_xticklabels([])

    ax2.grid()
    ax2.xaxis.set_major_locator(MultipleLocator(6))
    ax2.set_xticklabels([])

    ax3.grid()
    ax3.xaxis.set_major_locator(MultipleLocator(6))

    ax4.view_init(elev=22, azim=-45)
    ax4.set_box_aspect([1, 1, 1])
    return fig, ax0, ax1, ax2, ax3, ax4


def plot_4grid(title):
    fig = plt.figure(figsize=(7, 5), dpi=(300))
    fig.suptitle(title)
    gs = GridSpec(2, 2, figure=fig)
    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[0, 1])
    ax3 = fig.add_subplot(gs[1, 0])
    ax4 = fig.add_subplot(gs[1, 1])

    ax1.grid()
    ax1.xaxis.set_major_locator(MultipleLocator(6))

    ax2.grid()
    ax2.xaxis.set_major_locator(MultipleLocator(6))

    ax3.grid()
    ax3.xaxis.set_major_locator(MultipleLocator(6))

    ax4.grid()
    ax4.xaxis.set_major_locator(MultipleLocator(6))

    return fig, ax1, ax2, ax3, ax4


def plot_4grid_iterative(title):
    fig = plt.figure(figsize=(7, 5), dpi=(300))
    fig.suptitle(title)
    gs = GridSpec(2, 2, figure=fig)
    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[0, 1])
    ax3 = fig.add_subplot(gs[1, 0])
    ax4 = fig.add_subplot(gs[1, 1])

    ax1.grid()
    # ax1.xaxis.set_major_locator(MultipleLocator(6))

    ax2.grid()
    # ax2.xaxis.set_major_locator(MultipleLocator(6))

    ax3.grid()
    # ax3.xaxis.set_major_locator(MultipleLocator(6))
    ax3.set_xlabel(r'$t_0+t$ [hours]')

    ax4.grid()
    # ax4.xaxis.set_major_locator(MultipleLocator(6))
    ax4.set_xlabel(r'$t_0+t$ [hours]')
    return fig, [ax1, ax2, ax3, ax4]


def circadian(t, C, dC, dC2, title):
    plot_2d(title)
    plt.plot(t/m, C, '-', color='k', label='$C(t)$')
    plt.plot(t/m, dC, '--', color='k', label=r'$\frac{dC}{dt}$')
    # plt.plot(t/m, dC2, ':', color='tab:cyan', label='finite difference')
    plt.legend()
    #plt.tight_layout()
    plt.savefig("images/F/"+title+".png")


def phi(X, title):

    plot_2d(title)
    plt.plot(X[0]/m, X[0]/np.max(X[0]),
             ':', color='tab:blue', label=r'$\dfrac{X_0(t)}{\rm{max}(X_0(t))}$')
    plt.plot(X[0]/m, X[1],
             '-.', color='tab:blue', label=r'$X_1(t)$')
    plt.plot(X[0]/m, X[2],
             '--', color='tab:blue', label=r'$X_2(t)$')
    plt.plot(X[0]/m, X[3],
             '-', color='tab:blue', label=r'$X_3(t)$')
    plt.legend()
    #plt.tight_layout()
    plt.savefig("images/Phi/"+title+"_2d.png")

    fig, ax = plot_3d(title)
    ax.plot(X[1, 0], X[2, 0], X[3, 0],
            '*', color='tab:blue',
            label=fr"$\vec x_0$, at $t_0=${int(X[0, 0]/m)} h")
    ax.plot(X[1], X[2], X[3],
            color='tab:blue', label=r'$\vec x(t)$')
    ax.set_xlabel(r'$x_1$')
    ax.set_ylabel(r'$x_2$')
    ax.set_zlabel(r'$x_3$')
    fig.legend(loc='lower right')
    #plt.tight_layout()
    plt.savefig("images/Phi/"+title+"_3d.png")

    fig, ax0, ax1, ax2, ax3, ax4 = plot_grid(title)
    ax0.plot(X[0]/m, X[0],
             ':', color='tab:blue')
    ax0.plot(X[0, 0]/m, X[:, 0][0],
             '*', color='tab:blue')
    ax0.set_ylabel(r'$t_0+t$ [min]')
    ax1.plot(X[0]/m, X[1],
             '-.', color='tab:blue')
    ax1.plot(X[0, 0]/m, X[:, 0][1],
             '*', color='tab:blue')
    ax1.set_ylabel(r'$x_1$')
    ax2.plot(X[0]/m, X[2],
             '--', color='tab:blue')
    ax2.plot(X[0, 0]/m, X[:, 0][2],
             '*', color='tab:blue')
    ax2.set_ylabel(r'$x_2$')
    ax3.plot(X[0]/m, X[3],
             '-', color='tab:blue')
    ax3.plot(X[0, 0]/m, X[:, 0][3],
             '*', color='tab:blue')
    ax3.set_xlabel(r'$t_0+t$ [hours]')
    ax3.set_ylabel(r'$x_3$')
    ax4.plot(X[1], X[2], X[3],
             color='tab:blue', label=r'$\vec \gamma(t)$')
    ax4.plot(X[1, 0], X[2, 0], X[3, 0],
             '*', color='tab:blue',
             label=fr"$\vec x_0$, at $t_0=${int(X[0, 0]/m)} h")
    ax4.set_xlabel(r'$x_1$')
    ax4.set_ylabel(r'$x_2$')
    ax4.set_zlabel(r'$x_3$')
    fig.legend(loc='lower right')
    #plt.tight_layout()
    plt.savefig("images/Phi/"+title+"_grid.png")


def dist(norm, title):
    plot_2d(title)
    plt.plot(norm[0]/m, norm[1],
             color='k', label=r'$||\vec{x}(t)-\vec{x}(t-T)||$')
    plt.yscale('log')
    plt.legend()
    #plt.tight_layout()
    plt.savefig("images/Phi/"+title+"_norm.png")


def vecfield(X, F, title):
    plot_2d(title)
    plt.plot(X[0]/m, F[0],
             ':', color='tab:pink', label=r'$F_0$')
    plt.plot(X[0]/m, F[1],
             '-.', color='tab:pink', label=r'$F_1$')
    plt.plot(X[0]/m, F[2],
             '--', color='tab:pink', label=r'$F_2$')
    plt.plot(X[0]/m, F[3],
             '-', color='tab:pink', label=r'$F_3$')
    plt.legend()
    #plt.tight_layout()
    plt.savefig("images/F/"+title+"_2d.png")

    fig, ax0, ax1, ax2, ax3, ax4 = plot_grid(title)
    ax0.plot(X[0]/m, F[0],
             ':', color='tab:pink')
    ax0.plot(X[0, 0]/m, F[0, 0],
             '*', color='tab:pink')
    ax0.set_ylabel(r'$F_0$')
    ax1.plot(X[0]/m, F[1],
             '-.', color='tab:pink')
    ax1.plot(X[0, 0]/m, F[1, 0],
             '*', color='tab:pink')
    ax1.set_ylabel(r'$F_1$')
    ax2.plot(X[0]/m, F[2],
             '--', color='tab:pink')
    ax2.plot(X[0, 0]/m, F[2, 0],
             '*', color='tab:pink')
    ax2.set_ylabel(r'$F_2$')
    ax3.plot(X[0]/m, F[3],
             '-', color='tab:pink')
    ax3.plot(X[0, 0]/m, F[3, 0],
             '*', color='tab:pink')
    ax3.set_xlabel(r'$t_0+t$ [hours]')
    ax3.set_ylabel(r'$F_3$')
    ax4.plot(F[1], F[2], F[3],
              color='tab:pink', label=r'$\vec F(\vec X(t))$')
    ax4.plot(F[1, 0], F[2, 0], F[3, 0],
             '*', color='tab:pink',
             label=fr"$\vec F(\vec X_0)$, at $t_0=${int(X[0, 0]/m)} h")
    ax4.set_xlabel(r'$F_1$')
    ax4.set_ylabel(r'$F_2$')
    ax4.set_zlabel(r'$F_3$')
    fig.legend(loc='lower right')
    #plt.tight_layout()
    plt.savefig("images/F/"+title+"_grid.png")


def jphi_e_norm(t, norm, title):
    plot_2d(title)
    plt.plot(t/m, norm[0], '-',
             color='tab:orange',
             label=r'$||J\vec \Phi_{t, \vec X_0} \cdot \vec e_0||$')
    plt.plot(t/m, norm[1], '--',
             color='tab:green',
             label=r'$||J\vec \Phi_{t, \vec X_0} \cdot \vec e_1||$')
    plt.plot(t/m, norm[2], '-.',
             color='tab:red',
             label=r'$||J\vec \Phi_{t, \vec X_0} \cdot \vec e_2||$')
    plt.plot(t/m, norm[3], ':',
             color='tab:purple',
             label=r'$||J\vec \Phi_{t, \vec X_0} \cdot \vec e_3||$')
    plt.yscale('log')
    plt.legend()
    #plt.tight_layout()
    plt.savefig("images/JPhi/"+title+".png")


def matrix_norm(t, norm, matrix_name, save_name):
    plot_2d(r"norm of $"+matrix_name+"$")
    plt.plot(t/m, np.ones(len(t)),
             'k--', label='1')
    plt.plot(t/m, norm,
             'k-', label=r"$||{}||$".format(matrix_name))
    plt.yscale('log')
    plt.legend()
    #plt.tight_layout()


def jphi_check(t, check, title):
    plot_2d(title)
    plt.plot(t/m, check,
             label=r'$J\Phi_t(\vec X_0) * F(\vec X_0) - F(\Phi_t(\vec X_0))$')
    plt.legend()
    #plt.tight_layout()
    plt.savefig("images/JPhi/"+title+".png")


def matrix_elements(t, A, name, save_name, Legend='Yes'):
    fig, ax1, ax2, ax3, ax4 = plot_4grid(r"Column vectors of $"+name+"$")
    mark = ('-', '--', '-.', ':')
    dim = len(A)
    for i in range(dim):
        ax1.plot(t/m, A[i, 0], mark[i],
                 label=r"$\frac{{\partial {}_{}}}{{\partial X_{{0,0}}}}$"
                 .format(name[1:], i))
        if Legend == 'Yes':
            ax1.legend(loc='lower right')
    for i in range(dim):
        ax2.plot(t/m, A[i, 1], mark[i],
                 label=r"$\frac{{\partial {}_{}}}{{\partial X_{{0,1}}}}$"
                 .format(name[1:], i))
        if Legend == 'Yes':
            ax2.legend(loc='lower right')
    for i in range(dim):
        ax3.plot(t/m, A[i, 2], mark[i],
                 label=r"$\frac{{\partial {}_{}}}{{\partial X_{{0,2}}}}$"
                 .format(name[1:], i))
        if Legend == 'Yes':
            ax3.legend(loc='lower right')
    if dim > 3:
        for i in range(dim):
            ax4.plot(t/m, A[i, 3], mark[i],
                     label=r"$\frac{{\partial {}_{}}}{{\partial X_{{0,3}}}}$"
                     .format(name[1:], i))
            if Legend == 'Yes':
                ax4.legend(loc='lower right')


def matrix_elements2(t, A, name):
    fig, ax = plot_4grid_iterative(r"column vectors of "\
                                   r"$<"+name+"_i,"+name+"_j>$-matrix")
    mark = ('-', '--', '-.', ':')
    dim = len(A)
    for j in range(dim):
        for i in range(dim):
            ax[j].plot(t/m, A[i, j], mark[i],
                     label=r"$<\vec{{{}}}_{}, \vec{{{}}}_{}>$"
                     .format(name, i+1, name, j+1))
            ax[j].legend(loc='lower right')
            ax[j].xaxis.set_major_locator(MultipleLocator(6))


def matrix_elements3(t, A, name):
    fig, ax = plot_4grid_iterative(r"modulus and argument of "\
                                   r"$<{}, \vec{{N}}_j>$".format(name))
    mark = ('-', ':', '--')
    dim, steps = A.shape
    for i in range(dim):
        if np.all(np.imag(A[i]) == 0):
            ax[i].plot(t/m, A[i], mark[2],
                       label=r"$<{}, \vec{{N}}_{}>$"
                       .format(name, i+1))
            ax[i].legend(loc='lower right')
            ax[i].xaxis.set_major_locator(MultipleLocator(6))
        else:
            ax[i].plot(t/m, np.absolute(A[i]), mark[0],
                       label=r"$||<{}, \vec{{N}}_{}>||$"
                       .format(name, i+1))
            ax[i].set_ylabel(r"$||<{}, \vec{{N}}_{}>||$"
                       .format(name, i+1))
            ax[i].legend(loc='upper left')
            ax2 = ax[i].twinx()
            ax2.plot(t/m, np.unwrap(np.angle(A[i])), mark[1],
                       label=r"arg$(<{}, \vec{{N}}_{}>)$"
                       .format(name, i+1))
            ax2.set_ylabel(r"arg$(<{}, \vec{{N}}_{}>)$"
                           .format(name, i+1))
            ax2.legend(loc='lower right')
            ax[i].xaxis.set_major_locator(MultipleLocator(6))
    plt.tight_layout()


def jf_check(t, check, title):
    plot_2d(title)
    plt.plot(t/m, check,
             label=r"$F(X(t+\Delta t))-F(X(t))-\Delta t \cdot JF(X(t))"\
                   r" \cdot F(X(t))$")
    plt.legend()
    #plt.tight_layout()
    plt.savefig("images/JF/"+title+".png")


def matrix_eigenvalues_re(t, l, l_name, matrix_name, save_name):
    l = l.T
    mark = ('-', '-.', '--', ':')
    plot_2d(r"Real part of eigenvalues of $"+matrix_name+"$")
    for i in range(len(l)):
        plt.plot(t/m, np.real(l[i]),
                 mark[i], label=r'$\Re({}_{}$)'.format(l_name, i+1))
    plt.legend()
    #plt.tight_layout()

def matrix_eigenvalues_im(t, l, l_name, matrix_name, save_name):
    l = l.T
    mark = ('-', '-.', '--', ':')
    plot_2d(r"Imaginary part of eigenvalues of $"+matrix_name+"$")
    for i in range(len(l)):
        plt.plot(t/m, np.imag(l[i]),
                 mark[i], label=r'$\Im({}_{}$)'.format(l_name, i+1))
    plt.legend()
    #plt.tight_layout()
    

def matrix_eigenvectors_re(t, v, v_name, matrix_name, save_name, Legend='Yes'):
    mark = ('-', '-.', '--', ':')
    dim = len(v[0])
    fig, ax = plot_4grid_iterative(r"real part of eigenvectors of $"\
                                   +matrix_name+"$")
    for j in range(dim):
        for i in range(dim):
            ax[j].plot(t/m, np.real(v)[:, i, j],
                       mark[i], label=r"$\Re({}_{{{}, {}}})$".format(v_name, j+1, i))
            if Legend == 'Yes':
                ax[j].legend(loc='lower right')
            ax[i].xaxis.set_major_locator(MultipleLocator(6))


def matrix_eigenvectors_im(t, v, v_name, matrix_name, save_name, Legend='Yes'):
    mark = ('-', '-.', '--', ':')
    dim = len(v[0])
    fig, ax = plot_4grid_iterative(r"imaginary part of eigenvectors of $"\
                                   +matrix_name+"$")
    for j in range(dim):
        for i in range(dim):
            ax[j].plot(t/m, np.imag(v)[:, i, j],
                       mark[i], label=r"$\Im({}_{{{}, {}}})$".format(v_name, j+1, i))
            if Legend == 'Yes':
                ax[j].legend(loc='lower right')
            ax[i].xaxis.set_major_locator(MultipleLocator(6))


def v_projections(t, v, v_on_L1, v_on_L2, v_on_L3, v_on_L4, vec_name):
    plot_2d(r"projections of $"+vec_name+"(t)$")
    plt.plot(t/m, v_on_L1,
              label=r"$<{}(t), \vec{{\Lambda}}_1>$".format(vec_name))
    plt.plot(t/m, v_on_L2,
              label=r"$<{}(t), \vec{{\Lambda}}_2>$".format(vec_name))
    plt.plot(t/m, v_on_L3,
              label=r"$<{}(t), \vec{{\Lambda}}_3>$".format(vec_name))
    plt.plot(t/m, v_on_L4,
              label=r"$<{}(t), \vec{{\Lambda}}_4>$".format(vec_name))
    plt.plot(t/m, v_on_L1**2+v_on_L2**2+v_on_L3**2+v_on_L4**2,
              '--', label=r"$\sum_i <{}(t), \vec{{\Lambda}}_i>^2$".format(vec_name))
    plt.plot(t/m, LA.norm(v, axis=1)**2,
                ':', label=r"$<{}(t), {}(t)>$".format(vec_name, vec_name))
    plt.legend()


# def V_plane_projections(v_on_V, v_name, V_name):
#     fig = plt.figure(figsize=(7, 5), dpi=(300))
#     plt.title(r"${}(t)$-curve in the ${}$ plane".format(v_name, V_name))
#     plt.plot(np.real(v_on_V), np.imag(v_on_V))
#     plt.plot(np.real(v_on_V)[0], np.imag(v_on_V)[0],
#              '*', label="start")
#     plt.plot(np.real(v_on_V)[-1], np.imag(v_on_V)[-1],
#              'x', label="stop")
#     plt.grid()
#     plt.legend()
#     plt.xlabel(r"$\Re <{}(t), {}(t)>$".format(v_name, V_name))
#     plt.ylabel(r"$\Im <{}(t), {}(t)>$".format(v_name, V_name))


def V_plane_projections_grid(t, v_on_V, v_name, V_name):
    """
    v_on_V has shape (dim, steps)
    """
    lim = np.max(np.abs(v_on_V))*1.2
    fig, ax = plot_4grid_iterative(r"${}(t)$-curve in ${}$ planes"
                                   .format(v_name, V_name))
    for i in range(len(v_on_V)):
        if np.all(np.imag(v_on_V[i]) == 0):
            ax[i].plot(t/m, v_on_V[i])
            ax[i].set_xlabel(r"$t_0+t$")
            ax[i].set_ylabel(r"$<{}(t), {}_{}(t)>$"
                             .format(v_name, V_name, i+1))
            ax[i].xaxis.set_major_locator(MultipleLocator(6))
        else:
            ax[i].plot(np.real(v_on_V[i]), np.imag(v_on_V[i]))
            ax[i].plot(np.real(v_on_V[i])[0], np.imag(v_on_V[i])[0],
                       '*', label="start")
            ax[i].plot(np.real(v_on_V[i])[-1], np.imag(v_on_V[i])[-1],
                       'x', label="stop")
            ax[i].set_xlabel(r"$\Re <{}(t), {}_{}(t)>$"
                             .format(v_name, V_name, i+1))
            ax[i].set_ylabel(r"$\Im <{}(t), {}_{}(t)>$"
                             .format(v_name, V_name, i+1))
            ax[i].set_aspect('equal', 'box')
            ax[i].set(xlim=(-lim, lim), ylim=(-lim, lim))

    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    fig.legend(by_label.values(), by_label.keys(), loc='lower right')

    plt.tight_layout()


def V_plane_projections_grid2(v_on_V, v_name, V_name):
    """
    v_on_V has shape (dim, steps)
    """
    lim = np.max(np.abs(v_on_V))*1.2
    fig, ax = plot_4grid_iterative(r"${}(t)$-curve in ${}$ planes"
                                   .format(v_name, V_name))
    for i in range(len(v_on_V)):
        ax[i].plot(np.real(v_on_V[i]), np.imag(v_on_V[i]))
        ax[i].plot(np.real(v_on_V[i])[0], np.imag(v_on_V[i])[0],
                   '*', label="start")
        ax[i].plot(np.real(v_on_V[i])[-1], np.imag(v_on_V[i])[-1],
                   'x', label="stop")
        ax[i].set_xlabel(r"$\Re \frac{{<{}(t), {}_{}(t)>}}{{||{}||}}$"
                         .format(v_name, V_name, i+1, v_name))
        ax[i].set_ylabel(r"$\Im \frac{{<{}(t), {}_{}(t)>}}{{||{}||}}$"
                         .format(v_name, V_name, i+1, v_name))
    plt.setp(ax, aspect='equal', xlim=(-lim, lim), ylim=(-lim, lim))

    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    fig.legend(by_label.values(), by_label.keys(), loc='lower right')

    plt.tight_layout()


def V_plane_projections_grid3(v_in_V, v_name, V_name):
    """
    v_in_V has shape (steps, rows, columns)
    """
    steps, rows = v_in_V.shape
    lim = np.max(np.abs(v_in_V))*1.2
    fig, ax = plot_4grid_iterative(r"${}(t)$-curve in ${}$ basis"
                                   .format(v_name, V_name))
    for i in range(rows):
        ax[i].plot(np.real(v_in_V[:, i]), np.imag(v_in_V[:, i]))
        ax[i].plot(np.real(v_in_V[:, i])[0], np.imag(v_in_V[:, i])[0],
                   '*', label="start")
        ax[i].plot(np.real(v_in_V[:, i])[-1], np.imag(v_in_V[:, i])[-1],
                   'x', label="stop")
        ax[i].set_xlabel(r"$\Re \frac{{a_{}}}{{||{}||}}$"
                         .format(i+1, v_name))
        ax[i].set_ylabel(r"$\Im \frac{{a_{}}}{{||{}||}}$"
                         .format(i+1, v_name))
    plt.setp(ax, aspect='equal', xlim=(-lim, lim), ylim=(-lim, lim))

    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    fig.legend(by_label.values(), by_label.keys(), loc='lower right')

    plt.tight_layout()


# def real_and_imag(t, l, vec_name):
#      plot_2d("Real part of "+vec_name)
#      plt.plot(t/m, np.real(l[0]),
#               '-', label=r"$\Re(\vec{}_1(t)$)".format(vec_name))
#      plt.plot(t/m, np.real(l[1]),
#               '--', label=r"$\Re(\vec{}_2(t)$)".format(vec_name))
#      plt.plot(t/m, np.real(l[2]),
#               '-.', label=r"$\Re(\vec{}_3(t)$)".format(vec_name))
#      plt.plot(t/m, np.real(l[3]),
#               ':', label=r"$\Re(\vec{}_4(t)$)".format(vec_name))
#      plt.legend()

#      plot_2d("Imaginary part of "+vec_name)
#      plt.plot(t/m, np.imag(l[0]),
#               '-', label=r"$\Im(\vec{}_1(t)$)".format(vec_name))
#      plt.plot(t/m, np.imag(l[1]),
#               '--', label=r"$\Im(\vec{}_2(t)$)".format(vec_name))
#      plt.plot(t/m, np.imag(l[2]),
#               '-.', label=r"$\Im(\vec{}_3(t)$)".format(vec_name))
#      plt.plot(t/m, np.imag(l[3]),
#               ':', label=r"$\Im(\vec{}_4(t)$)".format(vec_name))
#      plt.legend()
