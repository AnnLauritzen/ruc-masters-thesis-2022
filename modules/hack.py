#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
An analysis module
"""
import numpy as np
import numpy.linalg as LA
import numpy.polynomial.polynomial as poly
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

from modules import hpa  # home made module
from modules import plot  # home made module

# time is in minutes
m = hpa.parameters()['m']
# period of gamma in minutes
T = hpa.parameters()['T']

def solve(f, t, x0, met='RK45'):
    met = ['RK45', 'RK23', 'Radau', 'BDF', 'LSODA']  # 'DOP853' is very slow
    met = met[0]
    tol = 1e-13
    sol = solve_ivp(fun=f, t_span=[t[0], t[-1]], y0=x0,
                    method=met, t_eval=t, rtol=tol, atol=tol)
    print(sol.message)
    check = np.sum(t==sol.t) == len(t)
    if np.all(check) is False:
        print('Predefined time span is different from the solvers time span')
    check = np.sum(t==sol.y[0]) == len(t)
    if np.all(check) is False:
        print('Predefined time span is different from the solution time')
    return sol


def dist_to_gamma(X):
    """
    a distance function
    D(f(t)) = ||f(t) - f(t-T)||
    where we expect D=0 when f(t) is the periodic orbit Gamma(t)
    """
    t = X[0, T:]
    norm = LA.norm(X[1:4, T:]-X[1:4, :-T], axis=0)
    return [t, norm]


def flow(days, X0):

    # define initial value:
    JPhi0 = np.identity(4)
    Y0 = np.concatenate((X0, JPhi0.reshape(4*4)))

    # define time span:
    t0 = X0[0]  # initial time [minutes]
    tr = T*days  # run time [minutes]
    tf = t0+tr  # final time [minutes]

    # Do not use numpy arange, it will give rounding errors:
    t = np.linspace(t0, tf, tr, endpoint=False)  # [minutes]

    # get solution X and JPhi simultaniously:
    sol = solve(hpa.derivative_of_JPhi, t, Y0)
    X = sol.y[:4]
    JPhi = sol.y[4:].reshape(4, 4, tr)

    norm = dist_to_gamma(X)

    return X, JPhi, norm


def get_sorted_eigen(A):

    if len(A.shape) == 3:
        [l, v] = LA.eig(np.transpose(A, axes=(2, 0, 1)))
        l_sorted = np.copy(l)
        v_sorted = np.copy(v)
        for i in range(T):
            l_sorted[i] = np.sort(l[i], axis=-1)
            v_sorted[i] = v[i, :, l[i].argsort()]
        l1 = l_sorted[:, 3]
        l2 = l_sorted[:, 2]
        l3 = l_sorted[:, 1]
        l4 = l_sorted[:, 0]
        v1 = v_sorted[:, 3]
        v2 = v_sorted[:, 2]
        v3 = v_sorted[:, 1]
        v4 = v_sorted[:, 0]

    elif len(A.shape) == 2:
        [l, v] = LA.eig(A)
        l_sorted = np.sort(l)
        v_sorted = v[:, l.argsort()]
        l1 = l_sorted[3]
        l2 = l_sorted[2]
        l3 = l_sorted[1]
        l4 = l_sorted[0]
        v1 = v_sorted[:, 3]
        v2 = v_sorted[:, 2]
        v3 = v_sorted[:, 1]
        v4 = v_sorted[:, 0]

    return [l1, l2, l3, l4], [v1, v2, v3, v4]


def alpha(v, M):
    """
    v has shape (steps, rows)
    M has shape (rows, cols, steps)
    """
    rows, cols, steps = M.shape
    Mv = np.matmul(np.transpose(M, axes=(2, 0, 1)), v[:, :, np.newaxis])
    Mv = np.squeeze(Mv)

    numerator = np.empty(steps, dtype=np.complex128)
    denominator = np.empty(steps)
    a = np.empty(steps)
    for i in range(steps):
        numerator[i] = np.inner(v[i], np.conj(Mv[i]))  # complex inner product
        denominator[i] = LA.norm(v[i])**2
        a[i] = np.real(numerator[i] / denominator[i])
    return a


def investigate_matrix(t, M, matrix_name, save_name):
    """
    t has shape (time)
    M has shape (dim, dim, time)
    """
    plot.matrix_elements(t, M, matrix_name, save_name)
    plt.savefig("images/"+save_name+"/columns of "+save_name+".png")

    if LA.matrix_rank(M[:, :, 0]) == len(M):
        plot.plot_2d(r"determinant of $"+matrix_name+"$")
        plt.plot(t/m, LA.det(np.transpose(M, axes=(2, 0, 1))))
        # plt.yscale('log')
        plt.savefig("images/"+save_name+"/determinant of "+save_name+".png")

    M_norm = LA.norm(M, axis=(0, 1))
    plot.matrix_norm(t, M_norm, matrix_name, save_name)
    plt.savefig("images/"+save_name+"/norm of "+save_name+".png")


def v_projections_on_V_basis(v, V):
    # steps, rows, cols = V.shape
    v_on_V1 = np.einsum('ji,ji->j', v, np.conj(V[:, :, 0]))
    v_on_V2 = np.einsum('ji,ji->j', v, np.conj(V[:, :, 1]))
    v_on_V3 = np.einsum('ji,ji->j', v, np.conj(V[:, :, 2]))
    v_on_V4 = np.einsum('ji,ji->j', v, np.conj(V[:, :, 3]))
    return np.array([v_on_V1, v_on_V2, v_on_V3, v_on_V4])


def make_eigvec_cont(M_v):
    steps, rows, cols = M_v.shape
    inner_product = np.empty(shape=(steps, cols), dtype=np.complex128)
    for i in range(steps):
        for j in range(cols):
            inner_product[i, j] = np.vdot(M_v[i, :, j], M_v[i-1, :, j])
            M_v[i, :, j] = inner_product[i, j]*M_v[i, :, j]
            M_v[i, :, j] = M_v[i, :, j]/LA.norm(M_v[i, :, j])
    return M_v, inner_product


def make_eigvec_orthogonal(M_v):
    steps, rows, cols = M_v.shape
    M_v_ort = np.copy(M_v)
    for k in range(cols):
        v = M_v[:, :, k]
        rv = np.real(v)
        iv = np.imag(v)
        angle = np.empty(shape=steps)
        for i in range(steps):
            angle[i] = 1/2*np.arctan(-2*np.inner(rv[i], iv[i])/
                                     (LA.norm(rv[i])**2-LA.norm(iv[i])**2))
            M_v_ort[i, :, k] = np.exp(1j*angle[i])*v[i]
    return M_v_ort
