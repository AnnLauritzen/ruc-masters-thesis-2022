#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
A sanity checking module
"""
import numpy as np
import numpy.linalg as lin
import numpy.polynomial.polynomial as poly
from scipy.integrate import solve_ivp

from modules import hpa  # home made module


def dxzdt(t, xz, p=hpa.parameters()):
    """
    In theory, F should solve the differential equation Z'=JF*Z
    """
    
    x = xz[:4]
    z = xz[4:]
    
    derivs = np.empty(shape=(8), dtype=float)
    derivs[:4] = hpa.vector_field(t, x)
    derivs[4:] = np.matmul(hpa.derivative_of_vector_field(t, x), z.T)
    
    return derivs


def sanity_check_JF(F, JF):
    """
    In theory, the following should be an approximation:
    F(X(t+dt)) = F(X(t)) + dt * JF(X(t)) * F(X(t))
    """

    dt = 1

    # F(X(t)):
    Ft = F.transpose()[:, :, np.newaxis]

    # F(X(t+dt)):
    Ftdt = np.roll(Ft, -dt, axis=0)

    check = Ftdt[0] == Ft[1]
    if np.all(check) is False:
        print('F(t+dt) is wrong!')
    check = Ft[0] == Ftdt[-1]
    if np.all(check) is False:
        print('F(t+dt) is wrong!')

    # JF(X(t)):
    JFt = np.transpose(JF, axes=(2, 0, 1))

    # F(X(t+dt)) - F(X(t)) - dt * JF(X(t)) * F(X(t)):
    diff = Ftdt - Ft - dt*np.matmul(JFt, Ft)

    return np.squeeze(diff)


def sanity_check_JPhi(JPhi, FX0, X):
    """
    In theory, the following should hold: 
    F(Phi_t(x0)) = JPhi_t(x0)*F(x0)
    """

    # F(Phi_t(X0)):
    FX = hpa.vector_field(X[0], X).T

    check = FX0 == FX[0]
    if check is False:
        print(r'We got the wrong FX0 in our sanity check!')

    JPhi = np.transpose(JPhi, axes=(2, 0, 1))
    diff = np.matmul(JPhi, FX0) - FX

    return diff.T

